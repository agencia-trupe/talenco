const config = {
    padrao: {
        toolbar: [['Bold', 'Italic']],
    },

    clean: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
    },

    cleanBr: {
        toolbar: [],
        removePlugins: 'toolbar,elementspath',
        enterMode: CKEDITOR.ENTER_BR,
    },

    trabalheConosco: {
        toolbar: [['Bold', 'Link', 'Unlink']],
        enterMode: CKEDITOR.ENTER_BR,
        height: 100,
    },

    sobreNos: {
        toolbar: [['Bold', 'Italic'], ['Link', 'Unlink']],
    },

    banner: {
        toolbar: [['Bold']],
        enterMode: CKEDITOR.ENTER_BR,
    },

    blog: {
        toolbar: [['Bold', 'Italic'], ['Link', 'Unlink'], ['InjectImage']],
        height: 600,
    },
};

export default function TextEditor() {
    CKEDITOR.config.language = 'pt-br';
    CKEDITOR.config.uiColor = '#dce4ec';
    CKEDITOR.config.contentsCss = [
        `${$('base').attr('href')}/assets/ckeditor.css`,
        CKEDITOR.config.contentsCss,
    ];
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.resize_enabled = false;
    CKEDITOR.plugins.addExternal('injectimage', `${$('base').attr('href')}/assets/injectimage/plugin.js`);
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraPlugins = 'injectimage';

    $('.ckeditor').each((i, obj) => {
        CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
    });
}
