import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('#form-newsletter').submit(function (event) {
    event.preventDefault();

    const $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: `${$('base').attr('href')}/newsletter`,
        data: {
            nome: $form.find('input[name=nome]').val(),
            email: $form.find('input[name=email]').val(),
        },
    })
        .done(() => {
            alert('Cadastro efetuado com sucesso!');
            $form[0].reset();
        })
        .fail((data) => {
            if (data.status !== 422) {
                return;
            }

            const res = data.responseJSON;
            const txt = [];
            let field = '';

            for (field in res) {
                res[field].map(error => txt.push(error));
            }

            alert(txt.join('\n'));
        })
        .always(() => {
            $form.removeClass('sending');
        });
});

$('#form-contato').submit(function (event) {
    event.preventDefault();

    const $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: `${$('base').attr('href')}/contato`,
        data: {
            nome: $form.find('input[name=nome]').val(),
            email: $form.find('input[name=email]').val(),
            telefone: $form.find('input[name=telefone]').val(),
            mensagem: $form.find('textarea[name=mensagem]').val(),
        },
    })
        .done(() => {
            alert('Mensagem enviada com sucesso!');
            $form[0].reset();
        })
        .fail((data) => {
            if (data.status !== 422) {
                return;
            }

            const res = data.responseJSON;
            const txt = [];
            let field = '';

            for (field in res) {
                res[field].map(error => txt.push(error));
            }

            alert(txt.join('\n'));
        })
        .always(() => {
            $form.removeClass('sending');
        });
});

$('.portfolio .thumb').click(function (event) {
    event.preventDefault();

    const id = $(this).data('projeto');

    $('.projeto-lightbox').hide();
    $(`.projeto-lightbox#projeto-${id} .imagens`).cycle({
        slides: '>.slide',
        timeout: 0,
    });
    $(`.projeto-lightbox#projeto-${id}`).show();
});

$('.projeto-lightbox .overlay').click(function () {
    $(this).parent().hide();
});

$('.blog-ver-mais').click(function (event) {
    event.preventDefault();

    let $btn = $(this),
        $container = $('.blog-posts');

    if ($btn.hasClass('loading')) return false;

    $btn.addClass('loading');

    $.get($btn.data('next'), (data) => {
        const posts = $(data.posts).hide();
        $container.append(posts);
        posts.fadeIn();

        $btn.removeClass('loading');

        if (data.nextPage) {
            $btn.data('next', data.nextPage);
        } else {
            $btn.fadeOut(() => {
                $this.remove();
            });
        }
    });
});

$('.banners').cycle({
    slides: '>.slide',
});

$('.depoimentos-cycle').cycle({
    slides: '>.depoimento',
    autoHeight: 'container',
});
