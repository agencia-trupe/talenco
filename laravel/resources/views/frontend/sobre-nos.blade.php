@extends('frontend.common.template')

@section('content')

    <div class="sobre-nos">
        <div class="bg" style="background-image:url({{ asset('assets/img/sobre-nos-bg/'.$imagem->imagem) }})"></div>

        <div class="center">
            <nav>
                <h2>SOBRE NÓS</h2>

                @foreach($paginas as $pag)
                <a href="{{ route('sobre-nos', $pag->slug) }}" @if($pagina->id == $pag->id) class="active" @endif>
                    {{ $pag->titulo }}
                </a>
                @endforeach
            </nav>

            <main>
                <h1>{{ $pagina->titulo }}</h1>
                {!! $pagina->texto !!}

                @if(count($pagina->topicos))
                <div class="topicos">
                @foreach($pagina->topicos as $topico)
                    <div class="topico">
                        <img src="{{ asset('assets/img/sobre-nos/'.$topico->imagem) }}" alt="">
                        <div class="texto">
                            <h2>{{ $topico->titulo }}</h2>
                            {!! $topico->texto !!}
                        </div>
                    </div>
                @endforeach
                </div>
                @endif
            </main>
        </div>
    </div>

@endsection
