@extends('frontend.common.template')

@section('content')

    <div class="o-que-fazemos">
        <section class="primary">
            <div class="center">
                <h2>O QUE FAZEMOS</h2>
                <p>{!! $oQueFazemos->introducao !!}</p>

                <div class="chamadas">
                    <div class="chamada">
                        <img src="{{ asset('assets/img/layout/icone-oqfazemos-executamos.png') }}" alt="">
                        <h3>{{ $oQueFazemos->chamada_1_titulo }}</h3>
                        <p>{!! $oQueFazemos->chamada_1_texto !!}</p>
                    </div>
                    <div class="chamada">
                        <img src="{{ asset('assets/img/layout/icone-oqfazemos-gerenciamento.png') }}" alt="">
                        <h3>{{ $oQueFazemos->chamada_2_titulo }}</h3>
                        <p>{!! $oQueFazemos->chamada_2_texto !!}</p>
                    </div>
                    <div class="chamada">
                        <img src="{{ asset('assets/img/layout/icone-oqfazemos-obra.png') }}" alt="">
                        <h3>{{ $oQueFazemos->chamada_3_titulo }}</h3>
                        <p>{!! $oQueFazemos->chamada_3_texto !!}</p>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="center">
                <h2>COMO FAZEMOS</h2>
                <p>{!! $oQueFazemos->como_fazemos !!}</p>

                <div class="servicos">
                    @foreach(range(1,4) as $i)
                    <div class="servico">
                        <h3>{{ $oQueFazemos->{'servico_'.$i.'_titulo'} }}</h3>
                        <p>{!! $oQueFazemos->{'servico_'.$i.'_texto'} !!}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <div class="imagem" style="background-image:url({{ asset('assets/img/o-que-fazemos/'.$oQueFazemos->imagem) }}">
            <div class="center">
                <div class="wrapper">
                    <h3>{{ $oQueFazemos->imagem_titulo }}</h3>
                    <p>{!! $oQueFazemos->imagem_texto !!}</p>
                    <img src="{{ asset('assets/img/layout/icone-acompanhamento-online.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
