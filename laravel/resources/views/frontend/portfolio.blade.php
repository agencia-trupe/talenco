@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="portfolio-categorias">
            <div class="center">
                <h1>PORTFÓLIO</h1>
                <nav>
                @foreach($categorias as $cat)
                    <a href="{{ route('portfolio', $cat->slug) }}" @if($categoria->id == $cat->id) class="active" @endif>
                        <span>{{ $cat->subtitulo ?: '&nbsp;' }}</span>
                        <span class="divider @if($cat->subtitulo) active @endif"></span>
                        {{ $cat->titulo }}
                    </a>
                @endforeach
                </nav>
            </div>
        </div>

        <div class="portfolio-thumbs">
            <div class="center">
                @foreach($categoria->portfolio as $projeto)
                <a href="#" class="thumb" data-projeto="{{ $projeto->id }}">
                    <img src="{{ asset('assets/img/portfolio/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <div class="wrapper">
                            {{ $projeto->titulo }}
                            @if($projeto->local || $projeto->area)
                            <small>
                                {{ $projeto->local }}
                                @if($projeto->local && $projeto->area)
                                -
                                @endif
                                {{ $projeto->area }}
                            </small>
                            @endif
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    @foreach($categoria->portfolio as $projeto)
    <div class="projeto-lightbox" style="display:none" id="projeto-{{ $projeto->id }}">
        <div class="overlay"></div>
        <div class="content">
            <div class="imagens">
                @foreach($projeto->imagens as $imagem)
                <div class="slide" style="background-image:url({{ asset('assets/img/portfolio/imagens/'.$imagem->imagem) }})"></div>
                @endforeach
                @if(count($projeto->imagens))
                <a href="#" class="controls cycle-next">next</a>
                <a href="#" class="controls cycle-prev">prev</a>
                @endif
            </div>
            <div class="descricao">
                <div class="left">
                    <h2>{{ $projeto->titulo }}</h2>
                    @if($projeto->local || $projeto->area)
                    <p>
                        {{ $projeto->local }}
                        @if($projeto->area && $projeto->local)
                        <br>
                        @endif
                        {{ $projeto->area }}
                    </p>
                    @endif
                    @if($projeto->arquiteto)
                    <p class="arquiteto">
                        Projeto: {{ $projeto->arquiteto }}
                    </p>
                    @endif
                </div>
                <div class="right">
                    <p>{!! $projeto->descricao !!}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach

@endsection
