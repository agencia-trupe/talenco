    <div class="newsletter">
        <div class="center">
            <form action="" id="form-newsletter">
                <p>Cadastre-se pare receber novidades</p>
                <div class="wrapper">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="email" name="email" placeholder="email" required>
                    <input type="submit" value="">
                </div>
            </form>
        </div>
    </div>

    <footer>
        <div class="center">
            <div class="informacoes">
                <div class="marca">
                    <img src="{{ asset('assets/img/layout/marca-talenco-rodape.png') }}" alt="">
                    Excelência em engenharia e construção

                    <div class="social">
                        @foreach(['facebook', 'linkedin', 'pinterest', 'instagram'] as $s)
                            @if($contato->{$s})
                                <a href="{{ Tools::parseLink($contato->{$s}) }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>

                <form action="" id="form-contato">
                    <input type="text" name="nome" placeholder="nome" required>
                    <input type="text" name="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" placeholder="telefone">
                    <textarea name="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                </form>

                <nav>
                    @include('frontend.common.nav')
                </nav>
            </div>

            <div class="unidades">
                @foreach($unidades as $unidade)
                    <p>
                        <strong>{{ $unidade->nome }} | {{ $unidade->telefone }}</strong><br>
                        {{ str_replace('<br />', ' - ', $unidade->endereco) }}
                    </p>
                @endforeach
            </div>

            <div class="copyright">
                <p>
                    &copy; {{ date('Y') }} {{ config('app.name') }} &middot; Todos os direitos reservados.
                    <span>|</span>
                    <a href="https://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
