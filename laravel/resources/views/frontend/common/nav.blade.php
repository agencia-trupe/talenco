<a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
<a href="{{ route('o-que-fazemos') }}" @if(Tools::routeIs('o-que-fazemos')) class="active" @endif>O que fazemos</a>
<a href="{{ route('portfolio') }}" @if(Tools::routeIs('portfolio')) class="active" @endif>Portfólio</a>
<a href="{{ route('sobre-nos') }}" @if(Tools::routeIs('sobre-nos')) class="active" @endif>Sobre nós</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
