    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <div class="social">
                @foreach(['facebook', 'linkedin', 'pinterest', 'instagram'] as $s)
                    @if($contato->{$s})
                        <a href="{{ Tools::parseLink($contato->{$s}) }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                    @endif
                @endforeach
            </div>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <div id="nav-mobile">
            @include('frontend.common.nav')
        </div>
    </header>
