@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})" class="slide">
                <div class="table">
                    <div class="table-cell">
                        <div class="texto">
                            <h2>{!! $banner->titulo !!}</h2>
                            <p>{!! $banner->texto !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="cycle-prev">prev</div>
            <div class="cycle-next">next</div>
        </div>

        <div class="center">
            <div class="frase">
                <h2>{{ $home->frase }}</h2>
            </div>

            @if(count($depoimentos))
            <div class="depoimentos">
                <div class="depoimentos-cycle">
                    @foreach($depoimentos as $depoimento)
                    <div class="depoimento">
                        <img src="{{ asset('assets/img/depoimentos/'.$depoimento->imagem) }}" alt="">
                        <div class="texto">
                            <p>{!! $depoimento->depoimento !!}</p>
                            <p class="desc">
                                {{ $depoimento->nome }}
                                @if($depoimento->cidade)
                                <br>{{ $depoimento->cidade }}
                                @endif
                            </p>
                        </div>
                    </div>
                    @endforeach
                    <a href="#" class="controls cycle-prev">prev</a>
                    <a href="#" class="controls cycle-next">next</a>
                </div>
            </div>
            @endif
        </div>

        <div class="chamadas">
            <div class="center">
                <div class="chamada chamada-1">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_1) }})"></div>
                    <div class="texto">
                        <h2>{{ $home->grid_titulo_1 }}</h2>
                    </div>
                </div>
                <a href="{{ $home->grid_link_2 }}" class="chamada chamada-2">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_2) }})"></div>
                    <div class="texto">
                        <h3>{{ $home->grid_titulo_2 }}</h3>
                        <p>{{ $home->grid_texto_2 }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
                <a href="{{ $home->grid_link_3 }}" class="chamada chamada-3">
                    <div class="texto">
                        <h3>{{ $home->grid_titulo_3 }}</h3>
                        <p>{{ $home->grid_texto_3 }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_3) }})"></div>
                </a>
                <a href="{{ $home->grid_link_4 }}" class="chamada chamada-4">
                    <div class="texto">
                        <h3>{{ $home->grid_titulo_4 }}</h3>
                        <p>{{ $home->grid_texto_4 }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_4) }})"></div>
                </a>
                <a href="{{ $home->grid_link_5 }}" class="chamada chamada-5">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_5) }})"></div>
                    <div class="texto">
                        <h3>{{ $home->grid_titulo_5 }}</h3>
                        <p>{{ $home->grid_texto_5 }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
                <a href="{{ $home->grid_link_6 }}" class="chamada chamada-6">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/home/'.$home->grid_imagem_6) }})"></div>
                    <div class="texto">
                        <h3>{{ $home->grid_titulo_6 }}</h3>
                        <p>{{ $home->grid_texto_6 }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
            </div>
        </div>
    </div>

    @if($home->exibir_blog && $blog && count($blog))
    <div class="blog-home">
        <div class="center">
            <div class="espaco-talenco">
                <span>ESPAÇO <strong>TALENCO</strong></span>
            </div>

            <div class="posts">
                @foreach($blog as $post)
                <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/blog/mais/'.$post->capa) }}" alt="">
                        <span>{{ Tools::formataData($post->data) }}</span>
                    </div>

                    <h3>{{ $post->titulo }}</h3>
                    <p>{{ Tools::chamadaPost($post->texto) }}</p>

                    <span class="mais">LER MAIS</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    @endif

@endsection
