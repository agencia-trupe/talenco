@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <p class="telefone">{{ $contato->telefone }}</p>

            <div class="wrapper">
                <div class="trabalhe-conosco">
                    <h3>TRABALHE CONOSCO</h3>
                    <p>{!! $contato->trabalhe_conosco !!}</p>
                </div>

                <form action="" id="form-contato">
                    <h3>FALE CONOSCO</h3>
                    <div class="col">
                        <input type="text" name="nome" placeholder="nome" required>
                        <input type="text" name="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" placeholder="telefone">
                    </div>
                    <div class="col">
                        <textarea name="mensagem" placeholder="mensagem" required></textarea>
                        <input type="submit" value="ENVIAR">
                    </div>
                </form>
            </div>
        </div>

        <div class="divider"></div>

        <div class="center">
            <h2>CONHEÇA NOSSAS UNIDADES</h2>

            <div class="unidades">
                @foreach($unidades as $unidade)
                <div class="unidade">
                    <h4>{{ $unidade->nome }}</h4>
                    <p><strong>{{ $unidade->telefone }}</strong></p>
                    <p>{!! $unidade->endereco !!}</p>
                    <div class="mapa">{!! $unidade->google_maps !!}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
