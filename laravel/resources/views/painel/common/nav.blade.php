<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs(['painel.home*', 'painel.banners*', 'painel.depoimentos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.depoimentos*')) class="active" @endif>
                <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
            </li>
        </ul>
    </li>
	<li @if(Tools::routeIs('painel.o-que-fazemos*')) class="active" @endif>
		<a href="{{ route('painel.o-que-fazemos.index') }}">O que fazemos</a>
	</li>
	<li @if(Tools::routeIs('painel.portfolio*')) class="active" @endif>
		<a href="{{ route('painel.portfolio.index') }}">Portfólio</a>
	</li>
	<li @if(Tools::routeIs('painel.sobre-nos*')) class="active" @endif>
		<a href="{{ route('painel.sobre-nos.index') }}">Sobre Nós</a>
    </li>
    <li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.blog.index') }}">Blog</a>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.contato*', 'painel.unidades*', 'painel.newsletter*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.unidades*')) class="active" @endif>
                <a href="{{ route('painel.unidades.index') }}">Unidades</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
</ul>
