@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.sobre-nos.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Sobre Nós
    </a>

    <legend>
        <h2><small>Sobre Nós /</small> Imagem de fundo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sobre-nos-bg.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sobre-nos-bg.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
