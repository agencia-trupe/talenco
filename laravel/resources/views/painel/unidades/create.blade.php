@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Unidades /</small> Adicionar Unidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.unidades.store', 'files' => true]) !!}

        @include('painel.unidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
