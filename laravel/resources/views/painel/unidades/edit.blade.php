@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Unidades /</small> Editar Unidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.unidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.unidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
