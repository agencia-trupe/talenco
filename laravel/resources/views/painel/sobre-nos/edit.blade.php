@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre Nós /</small> Editar Página</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sobre-nos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sobre-nos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
