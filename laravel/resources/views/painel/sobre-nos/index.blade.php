@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Sobre Nós
            <div class="btn-group pull-right">
                <a href="{{ route('painel.sobre-nos-bg.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Editar Imagem de Fundo</a>
                <a href="{{ route('painel.sobre-nos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Página</a>
            </div>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="sobre_nos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Tópicos</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->titulo }}</td>
                <td>
                    <a href="{{ route('painel.sobre-nos.topicos.index', $registro->id) }}" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-th-list" style="margin-right:10px;"></span>
                        Gerenciar
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.sobre-nos.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.sobre-nos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
