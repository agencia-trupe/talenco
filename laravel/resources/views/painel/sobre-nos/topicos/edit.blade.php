@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre Nós / {{ $pagina->titulo }} / Tópicos /</small> Editar Tópico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sobre-nos.topicos.update', $pagina->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sobre-nos.topicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
