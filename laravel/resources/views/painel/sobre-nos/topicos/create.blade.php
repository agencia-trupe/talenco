@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre Nós / {{ $pagina->titulo }} / Tópicos /</small> Adicionar Tópico</h2>
    </legend>

    {!! Form::open(['route' => ['painel.sobre-nos.topicos.store', $pagina->id], 'files' => true]) !!}

        @include('painel.sobre-nos.topicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
