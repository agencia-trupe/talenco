@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre Nós /</small> Adicionar Página</h2>
    </legend>

    {!! Form::open(['route' => 'painel.sobre-nos.store', 'files' => true]) !!}

        @include('painel.sobre-nos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
