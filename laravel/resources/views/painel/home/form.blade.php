@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_1', 'Grid Imagem 1') !!}
            @if($registro->grid_imagem_1)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_1', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_1', 'Grid Título 1') !!}
            {!! Form::text('grid_titulo_1', null, ['class' => 'form-control']) !!}
        </div>

        {{-- <div class="form-group">
            {!! Form::label('grid_link_1', 'Grid Link 1') !!}
            {!! Form::text('grid_link_1', null, ['class' => 'form-control']) !!}
        </div> --}}
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_2', 'Grid Imagem 2') !!}
            @if($registro->grid_imagem_2)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_2', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_2', 'Grid Título 2') !!}
            {!! Form::text('grid_titulo_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_texto_2', 'Grid Texto 2') !!}
            {!! Form::text('grid_texto_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_link_2', 'Grid Link 2') !!}
            {!! Form::text('grid_link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_3', 'Grid Imagem 3') !!}
            @if($registro->grid_imagem_3)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_3', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_3', 'Grid Título 3') !!}
            {!! Form::text('grid_titulo_3', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_texto_3', 'Grid Texto 3') !!}
            {!! Form::text('grid_texto_3', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_link_3', 'Grid Link 3') !!}
            {!! Form::text('grid_link_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_4', 'Grid Imagem 4') !!}
            @if($registro->grid_imagem_4)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_4', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_4', 'Grid Título 4') !!}
            {!! Form::text('grid_titulo_4', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_texto_4', 'Grid Texto 4') !!}
            {!! Form::text('grid_texto_4', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_link_4', 'Grid Link 4') !!}
            {!! Form::text('grid_link_4', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_5', 'Grid Imagem 5') !!}
            @if($registro->grid_imagem_5)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_5', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_5', 'Grid Título 5') !!}
            {!! Form::text('grid_titulo_5', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_texto_5', 'Grid Texto 5') !!}
            {!! Form::text('grid_texto_5', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_link_5', 'Grid Link 5') !!}
            {!! Form::text('grid_link_5', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('grid_imagem_6', 'Grid Imagem 6') !!}
            @if($registro->grid_imagem_6)
            <img src="{{ url('assets/img/home/'.$registro->grid_imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('grid_imagem_6', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_titulo_6', 'Grid Título 6') !!}
            {!! Form::text('grid_titulo_6', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_texto_6', 'Grid Texto 6') !!}
            {!! Form::text('grid_texto_6', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('grid_link_6', 'Grid Link 6') !!}
            {!! Form::text('grid_link_6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            <input type="hidden" name="exibir_blog" value="0">
            {!! Form::checkbox('exibir_blog', 1) !!}
            <span style="font-weight:bold">Exibir posts do Blog</span>
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
