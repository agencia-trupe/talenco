@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('introducao', 'Introdução') !!}
    {!! Form::textarea('introducao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_1_titulo', 'Chamada 1 Título') !!}
            {!! Form::text('chamada_1_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
            {!! Form::textarea('chamada_1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_2_titulo', 'Chamada 2 Título') !!}
            {!! Form::text('chamada_2_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::textarea('chamada_2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('chamada_3_titulo', 'Chamada 3 Título') !!}
            {!! Form::text('chamada_3_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('chamada_3_texto', 'Chamada 3 Texto') !!}
            {!! Form::textarea('chamada_3_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('como_fazemos', 'Como fazemos') !!}
    {!! Form::textarea('como_fazemos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('servico_1_titulo', 'Serviço 1 Título') !!}
            {!! Form::text('servico_1_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('servico_1_texto', 'Serviço 1 Texto') !!}
            {!! Form::textarea('servico_1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('servico_2_titulo', 'Serviço 2 Título') !!}
            {!! Form::text('servico_2_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('servico_2_texto', 'Serviço 2 Texto') !!}
            {!! Form::textarea('servico_2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('servico_3_titulo', 'Serviço 3 Título') !!}
            {!! Form::text('servico_3_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('servico_3_texto', 'Serviço 3 Texto') !!}
            {!! Form::textarea('servico_3_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {!! Form::label('servico_4_titulo', 'Serviço 4 Título') !!}
            {!! Form::text('servico_4_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('servico_4_texto', 'Serviço 4 Texto') !!}
            {!! Form::textarea('servico_4_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            @if($registro->imagem)
            <img src="{{ url('assets/img/o-que-fazemos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('imagem_titulo', 'Imagem Título') !!}
            {!! Form::text('imagem_titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('imagem_texto', 'Imagem Texto') !!}
            {!! Form::textarea('imagem_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
