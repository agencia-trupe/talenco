<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frase');
            $table->string('grid_imagem_1');
            $table->string('grid_titulo_1');
            $table->string('grid_link_1');
            $table->string('grid_imagem_2');
            $table->string('grid_titulo_2');
            $table->string('grid_texto_2');
            $table->string('grid_link_2');
            $table->string('grid_imagem_3');
            $table->string('grid_titulo_3');
            $table->string('grid_texto_3');
            $table->string('grid_link_3');
            $table->string('grid_imagem_4');
            $table->string('grid_titulo_4');
            $table->string('grid_texto_4');
            $table->string('grid_link_4');
            $table->string('grid_imagem_5');
            $table->string('grid_titulo_5');
            $table->string('grid_texto_5');
            $table->string('grid_link_5');
            $table->string('grid_imagem_6');
            $table->string('grid_titulo_6');
            $table->string('grid_texto_6');
            $table->string('grid_link_6');
            $table->boolean('exibir_blog');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
