<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreNosBgTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_nos_bg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre_nos_bg');
    }
}
