<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreNosTopicosTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_nos_topicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pagina_id')->unsigned()->nullable();
            $table->foreign('pagina_id')->references('id')->on('sobre_nos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('imagem');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre_nos_topicos');
    }
}
