<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration
{
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('telefone');
            $table->text('endereco');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('unidades');
    }
}
