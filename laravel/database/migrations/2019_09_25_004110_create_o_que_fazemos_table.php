<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOQueFazemosTable extends Migration
{
    public function up()
    {
        Schema::create('o_que_fazemos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('introducao');
            $table->string('chamada_1_titulo');
            $table->text('chamada_1_texto');
            $table->string('chamada_2_titulo');
            $table->text('chamada_2_texto');
            $table->string('chamada_3_titulo');
            $table->text('chamada_3_texto');
            $table->text('como_fazemos');
            $table->string('servico_1_titulo');
            $table->text('servico_1_texto');
            $table->string('servico_2_titulo');
            $table->text('servico_2_texto');
            $table->string('servico_3_titulo');
            $table->text('servico_3_texto');
            $table->string('servico_4_titulo');
            $table->text('servico_4_texto');
            $table->string('imagem');
            $table->string('imagem_titulo');
            $table->text('imagem_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('o_que_fazemos');
    }
}
