<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'frase' => '',
            'grid_imagem_1' => '',
            'grid_titulo_1' => '',
            'grid_link_1' => '',
            'grid_imagem_2' => '',
            'grid_titulo_2' => '',
            'grid_texto_2' => '',
            'grid_link_2' => '',
            'grid_imagem_3' => '',
            'grid_titulo_3' => '',
            'grid_texto_3' => '',
            'grid_link_3' => '',
            'grid_imagem_4' => '',
            'grid_titulo_4' => '',
            'grid_texto_4' => '',
            'grid_link_4' => '',
            'grid_imagem_5' => '',
            'grid_titulo_5' => '',
            'grid_texto_5' => '',
            'grid_link_5' => '',
            'grid_imagem_6' => '',
            'grid_titulo_6' => '',
            'grid_texto_6' => '',
            'grid_link_6' => '',
        ]);
    }
}
