<?php

use Illuminate\Database\Seeder;

class OQueFazemosSeeder extends Seeder
{
    public function run()
    {
        DB::table('o_que_fazemos')->insert([
            'introducao' => '',
            'chamada_1_titulo' => '',
            'chamada_1_texto' => '',
            'chamada_2_titulo' => '',
            'chamada_2_texto' => '',
            'chamada_3_titulo' => '',
            'chamada_3_texto' => '',
            'como_fazemos' => '',
            'servico_1_titulo' => '',
            'servico_1_texto' => '',
            'servico_2_titulo' => '',
            'servico_2_texto' => '',
            'servico_3_titulo' => '',
            'servico_3_texto' => '',
            'servico_4_titulo' => '',
            'servico_4_texto' => '',
            'imagem' => '',
            'imagem_titulo' => '',
            'imagem_texto' => '',
        ]);
    }
}
