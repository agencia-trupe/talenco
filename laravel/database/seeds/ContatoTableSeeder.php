<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone' => '+55 13 3395-0005',
            'email' => 'contato@talenco.com.br',
            'trabalhe_conosco' => 'Encaminhe seu currículo para o e-mail: <a href="mailto:rh@talenco.com.br">rh@talenco.com.br</a>'
        ]);
    }
}
