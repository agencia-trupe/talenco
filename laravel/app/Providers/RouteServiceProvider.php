<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('sobre-nos-bg', 'App\Models\SobreNosBg');
        $router->model('blog', 'App\Models\Post');
		$router->model('categorias_blog', 'App\Models\PostCategoria');
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('banners', 'App\Models\Banner');
		$router->model('home', 'App\Models\Home');
		$router->model('o-que-fazemos', 'App\Models\OQueFazemos');
		$router->model('portfolio', 'App\Models\Projeto');
		$router->model('categorias_portfolio', 'App\Models\ProjetoCategoria');
		$router->model('imagens_portfolio', 'App\Models\ProjetoImagem');
		$router->model('topicos', 'App\Models\SobreNosTopico');
		$router->model('sobre-nos', 'App\Models\SobreNosPagina');
        $router->model('unidades', 'App\Models\Unidade');
        $router->model('newsletter', 'App\Models\Newsletter');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('pagina_slug', function($value) {
            return \App\Models\SobreNosPagina::whereSlug($value)->firstOrFail();
        });
        $router->bind('categoria_slug', function($value) {
            return \App\Models\ProjetoCategoria::whereSlug($value)->firstOrFail();
        });
        $router->bind('blog_categoria_slug', function($value) {
            return \App\Models\PostCategoria::whereSlug($value)->firstOrFail();
        });
        $router->bind('post_slug', function($value) {
            return \App\Models\Post::whereSlug($value)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
