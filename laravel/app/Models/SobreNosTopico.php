<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SobreNosTopico extends Model
{
    protected $table = 'sobre_nos_topicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 160,
            'height' => 160,
            'path'   => 'assets/img/sobre-nos/'
        ]);
    }
}
