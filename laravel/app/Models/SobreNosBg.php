<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class SobreNosBg extends Model
{
    protected $table = 'sobre_nos_bg';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/sobre-nos-bg/'
        ]);
    }

}
