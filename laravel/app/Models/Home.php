<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_grid_imagem_1()
    {
        return CropImage::make('grid_imagem_1', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_grid_imagem_2()
    {
        return CropImage::make('grid_imagem_2', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_grid_imagem_3()
    {
        return CropImage::make('grid_imagem_3', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_grid_imagem_4()
    {
        return CropImage::make('grid_imagem_4', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_grid_imagem_5()
    {
        return CropImage::make('grid_imagem_5', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_grid_imagem_6()
    {
        return CropImage::make('grid_imagem_6', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/home/'
        ]);
    }

}
