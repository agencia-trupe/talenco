<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('o-que-fazemos', 'OQueFazemosController@index')->name('o-que-fazemos');
    Route::get('portfolio/{categoria_slug?}', 'PortfolioController@index')->name('portfolio');
    Route::get('sobre-nos/{pagina_slug?}', 'SobreNosController@index')->name('sobre-nos');
    Route::get('espaco-talenco/{blog_categoria_slug?}', 'BlogController@index')->name('blog');
    Route::get('espaco-talenco/{blog_categoria_slug}/{post_slug}', 'BlogController@show')->name('blog.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('sobre-nos-bg', 'SobreNosBgController', ['only' => ['index', 'update']]);
        Route::resource('blog/categorias', 'BlogCategoriasController', ['parameters' => ['categorias' => 'categorias_blog']]);
		Route::resource('blog', 'BlogController');
		Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('banners', 'BannersController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('o-que-fazemos', 'OQueFazemosController', ['only' => ['index', 'update']]);
		Route::resource('portfolio/categorias', 'PortfolioCategoriasController', ['parameters' => ['categorias' => 'categorias_portfolio']]);
		Route::resource('portfolio', 'PortfolioController');
		Route::get('portfolio/{portfolio}/imagens/clear', [
			'as'   => 'painel.portfolio.imagens.clear',
			'uses' => 'PortfolioImagensController@clear'
		]);
		Route::resource('portfolio.imagens', 'PortfolioImagensController', ['parameters' => ['imagens' => 'imagens_portfolio']]);
		Route::resource('sobre-nos.topicos', 'SobreNosTopicosController');
		Route::resource('sobre-nos', 'SobreNosController');
		Route::resource('unidades', 'UnidadesController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('newsletter', 'NewsletterController', ['only' => ['index', 'destroy']]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')->name('painel.newsletter.exportar');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
