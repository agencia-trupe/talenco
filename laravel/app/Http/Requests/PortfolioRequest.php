<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PortfolioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'portfolio_categoria_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'local' => '',
            'area' => '',
            'arquiteto' => '',
            'descricao' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
