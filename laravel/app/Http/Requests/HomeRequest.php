<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'frase' => 'required',
            'grid_imagem_1' => 'image',
            'grid_titulo_1' => 'required',
            'grid_link_1' => '',
            'grid_imagem_2' => 'image',
            'grid_titulo_2' => 'required',
            'grid_texto_2' => 'required',
            'grid_link_2' => '',
            'grid_imagem_3' => 'image',
            'grid_titulo_3' => 'required',
            'grid_texto_3' => 'required',
            'grid_link_3' => '',
            'grid_imagem_4' => 'image',
            'grid_titulo_4' => 'required',
            'grid_texto_4' => 'required',
            'grid_link_4' => '',
            'grid_imagem_5' => 'image',
            'grid_titulo_5' => 'required',
            'grid_texto_5' => 'required',
            'grid_link_5' => '',
            'grid_imagem_6' => 'image',
            'grid_titulo_6' => 'required',
            'grid_texto_6' => 'required',
            'grid_link_6' => '',
        ];
    }
}
