<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SobreNosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
