<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'blog_categoria_id' => 'required',
            'data' => 'required',
            'titulo' => 'required',
            'slug' => 'required',
            'capa' => 'required|image',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
