<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UnidadesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'telefone' => 'required',
            'endereco' => 'required',
            'google_maps' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
