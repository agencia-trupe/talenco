<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OQueFazemosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'introducao' => 'required',
            'chamada_1_titulo' => 'required',
            'chamada_1_texto' => 'required',
            'chamada_2_titulo' => 'required',
            'chamada_2_texto' => 'required',
            'chamada_3_titulo' => 'required',
            'chamada_3_texto' => 'required',
            'como_fazemos' => 'required',
            'servico_1_titulo' => 'required',
            'servico_1_texto' => 'required',
            'servico_2_titulo' => 'required',
            'servico_2_texto' => 'required',
            'servico_3_titulo' => 'required',
            'servico_3_texto' => 'required',
            'servico_4_titulo' => 'required',
            'servico_4_texto' => 'required',
            'imagem' => 'image',
            'imagem_titulo' => 'required',
            'imagem_texto' => 'required',
        ];
    }
}
