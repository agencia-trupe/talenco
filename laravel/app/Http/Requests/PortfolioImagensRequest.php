<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PortfolioImagensRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'required|image'
        ];
    }
}
