<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreNosRequest;
use App\Http\Controllers\Controller;

use App\Models\SobreNosPagina;

class SobreNosController extends Controller
{
    public function index()
    {
        $registros = SobreNosPagina::ordenados()->get();

        return view('painel.sobre-nos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.sobre-nos.create');
    }

    public function store(SobreNosRequest $request)
    {
        try {

            $input = $request->all();

            SobreNosPagina::create($input);

            return redirect()->route('painel.sobre-nos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(SobreNosPagina $registro)
    {
        return view('painel.sobre-nos.edit', compact('registro'));
    }

    public function update(SobreNosRequest $request, SobreNosPagina $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.sobre-nos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(SobreNosPagina $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.sobre-nos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
