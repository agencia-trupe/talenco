<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BlogRequest;
use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\PostCategoria;

class BlogController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = PostCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (PostCategoria::find($filtro)) {
            $registros = Post::where('blog_categoria_id', $filtro)->orderBy('id', 'DESC')->get();
        } else {
            $registros = Post::ordenados()->get();
        }

        return view('painel.blog.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.blog.create', compact('categorias'));
    }

    public function store(BlogRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            $input['slug'] = str_slug($input['slug']);

            if (count(Post::where('slug', $input['slug'])->get())) {
                throw new \Exception('O slug inserido já existe.', 1);
            }

            Post::create($input);

            return redirect()->route('painel.blog.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function edit(Post $registro)
    {
        $categorias = $this->categorias;

        return view('painel.blog.edit', compact('registro', 'categorias'));
    }

    public function update(BlogRequest $request, Post $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Post::upload_capa();

            $input['slug'] = str_slug($input['slug']);

            if (count(Post::where('id', '!=', $registro->id)->where('slug', $input['slug'])->get())) {
                throw new \Exception('O slug inserido já existe.', 1);
            }

            $registro->update($input);

            return redirect()->route('painel.blog.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function destroy(Post $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.blog.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
