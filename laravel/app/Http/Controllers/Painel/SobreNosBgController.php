<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreNosBgRequest;
use App\Http\Controllers\Controller;

use App\Models\SobreNosBg;

class SobreNosBgController extends Controller
{
    public function index()
    {
        $registro = SobreNosBg::first();

        return view('painel.sobre-nos-bg.edit', compact('registro'));
    }

    public function update(SobreNosBgRequest $request, SobreNosBg $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SobreNosBg::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.sobre-nos-bg.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
