<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreNosTopicosRequest;
use App\Http\Controllers\Controller;
use App\Models\SobreNosPagina;
use App\Models\SobreNosTopico;

class SobreNosTopicosController extends Controller
{
    public function index(SobreNosPagina $pagina)
    {
        $registros = $pagina->topicos()->ordenados()->get();

        return view('painel.sobre-nos.topicos.index', compact('pagina', 'registros'));
    }

    public function create(SobreNosPagina $pagina)
    {
        return view('painel.sobre-nos.topicos.create', compact('pagina'));
    }

    public function store(SobreNosTopicosRequest $request, SobreNosPagina $pagina)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SobreNosTopico::upload_imagem();

            $pagina->topicos()->create($input);

            return redirect()->route('painel.sobre-nos.topicos.index', $pagina->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(SobreNosPagina $pagina, SobreNosTopico $registro)
    {
        return view('painel.sobre-nos.topicos.edit', compact('registro', 'pagina'));
    }

    public function update(SobreNosTopicosRequest $request, SobreNosPagina $pagina, SobreNosTopico $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SobreNosTopico::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.sobre-nos.topicos.index', $pagina->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(SobreNosPagina $pagina, SobreNosTopico $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.sobre-nos.topicos.index', $pagina->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
