<?php

namespace App\Http\Controllers;

use App\Models\Newsletter;
use App\Http\Requests\NewsletterRequest;
use App\Models\Home;
use App\Models\Banner;
use App\Models\Depoimento;
use App\Models\Post;

class HomeController extends Controller
{
    public function index()
    {
        $home        = Home::first();
        $banners     = Banner::ordenados()->get();
        $depoimentos = Depoimento::ordenados()->get();
        $blog        = Post::ordenados()->take(3)->get();

        return view('frontend.home', compact('home', 'banners', 'depoimentos', 'blog'));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());
        return response('', 200);
    }
}
