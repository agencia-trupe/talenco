<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ProjetoCategoria;

class PortfolioController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        if (! $categorias->count()) {
            abort('404');
        }

        if (! $categoria->exists) {
            $categoria = $categorias->first();
        }

        return view('frontend.portfolio', compact('categorias', 'categoria'));
    }
}
