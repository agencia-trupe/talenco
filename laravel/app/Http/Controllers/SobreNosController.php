<?php

namespace App\Http\Controllers;

use App\Models\SobreNosBg;
use App\Models\SobreNosPagina;

class SobreNosController extends Controller
{
    public function index(SobreNosPagina $pagina)
    {
        $paginas = SobreNosPagina::ordenados()->get();
        $imagem  = SobreNosBg::first();

        if (! $paginas->count()) {
            abort('404');
        }

        if (! $pagina->exists) {
            $pagina = $paginas->first();
        }

        return view('frontend.sobre-nos', compact('paginas', 'pagina', 'imagem'));
    }
}
