<?php

namespace App\Http\Controllers;

use App\Models\OQueFazemos;

class OQueFazemosController extends Controller
{
    public function index()
    {
        $oQueFazemos = OQueFazemos::first();

        return view('frontend.o-que-fazemos', compact('oQueFazemos'));
    }
}
